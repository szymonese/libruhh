const express = require('express')
const sessions = require('express-session')
const app = express()

app.use(express.static('public'))
app.use(express.urlencoded({extended: true}))

app.set('view engine', 'ejs')

const age = 1000 * 60 * 60 ; //godzina
app.use(sessions({
    secret: "secretlibruh",
    saveUninitialized:true,
    cookie: { maxAge: age },
    resave: false 
}));



// strona głowna
app.get('/', (req, res) => {
    res.render('index')
})

// router dla zalogowanych
const loggedRouter = require('./routes/logged')
app.use('/logged', loggedRouter)


// router dla niezalogowanych
const loginRouter = require('./routes/login')
app.use('/login', loginRouter)

app.listen(3000)
