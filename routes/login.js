const express = require('express')

const router = express.Router()

    router.get('/', (req, res) => {
        //jesli sesja jest niezdefiniowana przekieruj na strone logowania
        if (req.session.logged === undefined) {
            res.render('login/login')
        } else {
            res.redirect('/logged')
        }
    })


    //logowanie 
    router.post('/', (req, res) => {
        const login = req.body.login
        const password = req.body.password

        if (login === 'admin' && password === 'admin') {
            req.session.logged = true
            res.redirect('/logged')
        } else {
            res.redirect('/login')
        }
    })

    
module.exports = router