const express = require('express')

const router = express.Router()

    router.get('/', (req, res) => {
        // if session is undefined
        if (req.session.logged === undefined) {
            res.redirect('/login')
        } else {
            res.render('logged/main_info')
        }
    })

module.exports = router